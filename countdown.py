#! /usr/bin/env/ python
# -*- coding: utf-8 -*-

import time
import curses
import locale

locale.setlocale(locale.LC_ALL, '')

timeleft = 1
timestart = time.time()
xpos = 92
ypos = 15

screen = curses.initscr()
curses.cbreak(1)
screen.nodelay(1)
screen.keypad(1)
curses.noecho()
curses.curs_set(0)
curses.start_color()
curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_MAGENTA)
curses.init_pair(2, curses.COLOR_GREEN, curses.COLOR_BLACK)
curses.init_pair(3, curses.COLOR_MAGENTA, curses.COLOR_BLACK)

screen.clear()


while True:
    screen.refresh()
    event = screen.getch()
    if event == 27:
        curses.nocbreak()
        screen.keypad(0)
        curses.echo()
        curses.endwin()
        print timeleft
        quit()
    else:
        passedtime = time.time() - timestart
        timeleft = (3600 - passedtime) / 60
        time.sleep(0.01)
        timecharlen = len(str(timeleft))
        char = 0
        nextchar = 0
        strtime = str(timeleft)
        M = '0' 
        while char < 5:
            if strtime[char] == '.':
                screen.addstr(15 + ypos,10 + nextchar + xpos, '    ', curses.color_pair(3))
                screen.addstr(16 + ypos,10 + nextchar + xpos, ' ' +M+M+ ' ', curses.color_pair(3))
                screen.addstr(17 + ypos,10 + nextchar + xpos, '    ', curses.color_pair(3))
                screen.addstr(18 + ypos,10 + nextchar + xpos, ' ' +M+M+ ' ', curses.color_pair(3))
                screen.addstr(19 + ypos,10 + nextchar + xpos, '    ', curses.color_pair(3))
            if strtime[char] == '0':
                screen.addstr(15 + ypos,10 + nextchar + xpos, M+M+M+M, curses.color_pair(3))
                screen.addstr(16 + ypos,10 + nextchar + xpos, M+'  '+M, curses.color_pair(3))
                screen.addstr(17 + ypos,10 + nextchar + xpos, M+'  '+M, curses.color_pair(3))
                screen.addstr(18 + ypos,10 + nextchar + xpos, M+'  '+M, curses.color_pair(3))
                screen.addstr(19 + ypos,10 + nextchar + xpos, M+M+M+M, curses.color_pair(3))
            if strtime[char] == '1':
                screen.addstr(15 + ypos,10 + nextchar + xpos, '   '+M, curses.color_pair(3))
                screen.addstr(16 + ypos,10 + nextchar + xpos, '   '+M, curses.color_pair(3))
                screen.addstr(17 + ypos,10 + nextchar + xpos, '   '+M, curses.color_pair(3))
                screen.addstr(18 + ypos,10 + nextchar + xpos, '   '+M, curses.color_pair(3))
                screen.addstr(19 + ypos,10 + nextchar + xpos, '   '+M, curses.color_pair(3))
            if strtime[char] == '2':
                screen.addstr(15 + ypos,10 + nextchar + xpos, M+M+M+M, curses.color_pair(3))
                screen.addstr(16 + ypos,10 + nextchar + xpos, '   '+M, curses.color_pair(3))
                screen.addstr(17 + ypos,10 + nextchar + xpos, M+M+M+M, curses.color_pair(3))
                screen.addstr(18 + ypos,10 + nextchar + xpos, M+'   ', curses.color_pair(3))
                screen.addstr(19 + ypos,10 + nextchar + xpos, M+M+M+M, curses.color_pair(3))
            if strtime[char] == '3':
                screen.addstr(15 + ypos,10 + nextchar + xpos, M+M+M+M, curses.color_pair(3))
                screen.addstr(16 + ypos,10 + nextchar + xpos, '   '+M, curses.color_pair(3))
                screen.addstr(17 + ypos,10 + nextchar + xpos, M+M+M+M, curses.color_pair(3))
                screen.addstr(18 + ypos,10 + nextchar + xpos, '   '+M, curses.color_pair(3))
                screen.addstr(19 + ypos,10 + nextchar + xpos, M+M+M+M, curses.color_pair(3))
            if strtime[char] == '4':
                screen.addstr(15 + ypos,10 + nextchar + xpos, M+'  '+M, curses.color_pair(3))
                screen.addstr(16 + ypos,10 + nextchar + xpos, M+'  '+M, curses.color_pair(3))
                screen.addstr(17 + ypos,10 + nextchar + xpos, M+M+M+M, curses.color_pair(3))
                screen.addstr(18 + ypos,10 + nextchar + xpos, '   '+M, curses.color_pair(3))
                screen.addstr(19 + ypos,10 + nextchar + xpos, '   '+M, curses.color_pair(3))
            if strtime[char] == '5':
                screen.addstr(15 + ypos,10 + nextchar + xpos, M+M+M+M, curses.color_pair(3))
                screen.addstr(16 + ypos,10 + nextchar + xpos, M+'   ', curses.color_pair(3))
                screen.addstr(17 + ypos,10 + nextchar + xpos, M+M+M+M, curses.color_pair(3))
                screen.addstr(18 + ypos,10 + nextchar + xpos, '   '+M, curses.color_pair(3))
                screen.addstr(19 + ypos,10 + nextchar + xpos, M+M+M+M, curses.color_pair(3))
            if strtime[char] == '6':
                screen.addstr(15 + ypos,10 + nextchar + xpos, M+M+M+M, curses.color_pair(3))
                screen.addstr(16 + ypos,10 + nextchar + xpos, M+'   ', curses.color_pair(3))
                screen.addstr(17 + ypos,10 + nextchar + xpos, M+M+M+M, curses.color_pair(3))
                screen.addstr(18 + ypos,10 + nextchar + xpos, M+'  '+M, curses.color_pair(3))
                screen.addstr(19 + ypos,10 + nextchar + xpos, M+M+M+M, curses.color_pair(3))
            if strtime[char] == '7':
                screen.addstr(15 + ypos,10 + nextchar + xpos, M+M+M+M, curses.color_pair(3))
                screen.addstr(16 + ypos,10 + nextchar + xpos, '   '+M, curses.color_pair(3))
                screen.addstr(17 + ypos,10 + nextchar + xpos, '   '+M, curses.color_pair(3))
                screen.addstr(18 + ypos,10 + nextchar + xpos, '   '+M, curses.color_pair(3))
                screen.addstr(19 + ypos,10 + nextchar + xpos, '   '+M, curses.color_pair(3))
            if strtime[char] == '8':
                screen.addstr(15 + ypos,10 + nextchar + xpos, M+M+M+M, curses.color_pair(3))
                screen.addstr(16 + ypos,10 + nextchar + xpos, M+'  '+M, curses.color_pair(3))
                screen.addstr(17 + ypos,10 + nextchar + xpos, M+M+M+M, curses.color_pair(3))
                screen.addstr(18 + ypos,10 + nextchar + xpos, M+'  '+M, curses.color_pair(3))
                screen.addstr(19 + ypos,10 + nextchar + xpos, M+M+M+M, curses.color_pair(3))
            if strtime[char] == '9':
                screen.addstr(15 + ypos,10 + nextchar + xpos, M+M+M+M, curses.color_pair(3))
                screen.addstr(16 + ypos,10 + nextchar + xpos, M+'  '+M, curses.color_pair(3))
                screen.addstr(17 + ypos,10 + nextchar + xpos, M+M+M+M, curses.color_pair(3))
                screen.addstr(18 + ypos,10 + nextchar + xpos, '   '+M, curses.color_pair(3))
                screen.addstr(19 + ypos,10 + nextchar + xpos, '   '+M, curses.color_pair(3))
            char = char + 1
            nextchar = nextchar + 5

