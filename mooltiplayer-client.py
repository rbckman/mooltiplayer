#!/usr/bin/env/ python
# -*- coding: utf-8 -*-

#mooltiplayer client v0.1
#Made for Makeitout Room Escape Game in Vaasa, Finland
#Using two Raspberry pi's as clients
#code by rbckman
#freesoftware, copy,share&remix=care
#<3

import subprocess
from subprocess import Popen
import os
import curses
import time
import pexpect
import cPickle as pickle
import config
import socket

global videolanguage, clientup, command, clients, clientpath, nextstatus, event

server = config.server
port = config.port
clientpath = config.clientpath
clientup = ""
event = ""
nextstatus = ''
videoplaying = 0
timestart = 1
timeleft = 1
countdownstarted = False

os.system('clear')
print "-------------mooltiplayer------------<3"

def initcurses():
    global stdscr
    stdscr = curses.initscr()
    curses.cbreak(1)
    stdscr.keypad(1)
    curses.noecho()
    stdscr.nodelay(1)
    curses.curs_set(0)
    curses.start_color()
    curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_MAGENTA)
    curses.init_pair(2, curses.COLOR_GREEN, curses.COLOR_BLACK)
    curses.init_pair(3, curses.COLOR_MAGENTA, curses.COLOR_BLACK)

def killcurses():
    global stdscr
    curses.nocbreak()
    stdscr.keypad(0)
    curses.echo()
    curses.endwin()

##--------------Choose Language---------------------------------

def language():
    language = 'svenska' , 'suomi', 'english'
    selected = 0
    while True:
        stdscr.clear()
        stdscr.addstr(2,2, "Choose video language", curses.color_pair(1))
        stdscr.addstr(4,2, "    Svenska", curses.color_pair(2))
        stdscr.addstr(5,2, "    Suomi", curses.color_pair(2))
        stdscr.addstr(6,2, "    English", curses.color_pair(2))
        stdscr.addstr(selected + 4,2, "-->", curses.color_pair(2))
        event = stdscr.getch()
        if event == curses.KEY_UP:
            if selected > 0:
                selected = selected - 1
        if event == curses.KEY_DOWN:
            if selected < 2:
                selected = selected + 1
        if event == 10:
            stdscr.clear()
            stdscr.refresh()
            return language[selected]
            os.system('clear')

##---------------Connection----------------------------------------------

def sendtoserver(host, port, data):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    while True:
        try:
            s.connect((host, port))
            s.send(data)
            break
        except:
            stdscr.addstr(20,2, "Connecting to server...", curses.color_pair(1))
    stdscr.addstr(20,2, "Sent to server..", curses.color_pair(1))
    s.close()

##---------------Makeitout Video Controller-----------------------------<3

def refresh():
    stdscr.clear()
    stdscr.addstr(2,2, ">>>--- Makeitout Video Controller ---<<<   ", curses.color_pair(1)) 
    stdscr.addstr(4,2, "Pirate language is set to " + videolanguage + "", curses.color_pair(2))
    if nextstatus == '':
        stdscr.addstr(6,2, "Press enter to hack time and send clients back to 1604... ", curses.color_pair(2))
    stdscr.refresh()

##----------------------Main loop starts-------------------------

initcurses()
videolanguage = language()
sendtoserver(server[0], port, videolanguage)
sendtoserver(server[1], port, videolanguage)
stdscr.clear()
refresh()
selected = 0

while True:
    refresh()
    if nextstatus != '':
        stdscr.addstr(6,2,  "Clients sent to 1604. ", curses.color_pair(2))
        stdscr.addstr(9,2,  "     Drown'em, arrr!", curses.color_pair(3))
        stdscr.addstr(10,2, "     Restart the game", curses.color_pair(3))
        stdscr.addstr(12,2, "Time left: " + str(timeleft)[0:5], curses.color_pair(3))
        stdscr.addstr(selected + 9 ,2, "-->", curses.color_pair(2))

    #--------LISTEN FOR INPUT------
    event = stdscr.getch()
    if event == 27:
        nextstatus = ''
        curses.nocbreak()
        killcurses()
        quit()
    elif event == curses.KEY_UP:
        if selected > 0:
            selected = selected - 1
    elif event == curses.KEY_DOWN:
        if selected < 1:
            selected = selected + 1
    elif event == 10:
        if videoplaying == False:
            nextstatus = 'play'
            timestart = time.time()
            countdownstarted = True
            videoplaying = True
            sendtoserver(server[0], port, nextstatus) 
            sendtoserver(server[1], port, nextstatus) 
            sendtoserver(server[2], port, nextstatus) 
            stdscr.clear()
            stdscr.addstr(2,2, "starting Time Travel Pro..", curses.color_pair(1))
        elif selected == 0 and videoplaying == True:
            stdscr.addstr(9,24,  "Really, are you mad? (y)es (no)", curses.color_pair(3))
            stdscr.refresh()
            while True:
                event = stdscr.getch()
                if event == ord('y'):
                    nextstatus = 'next'
                    stdscr.clear()
                    stdscr.addstr(2,2, "starting next videos...", curses.color_pair(1))
                    videoplaying = True
                    sendtoserver(server[0], port, nextstatus) 
                    sendtoserver(server[1], port, nextstatus) 
                    break
                if event == ord('n'):
                    break
        elif selected == 1 and videoplaying == True:
            stdscr.addstr(10,24,  "Really, restart? (y)es (no)", curses.color_pair(3))
            stdscr.refresh()
            while True:
                event = stdscr.getch()
                if event == ord('y'):
                    nextstatus = 'restart'
                    stdscr.clear()
                    stdscr.addstr(2,2, "restarting...", curses.color_pair(1)) 
                    sendtoserver(server[0], port, nextstatus)
                    sendtoserver(server[1], port, nextstatus)
                    sendtoserver(server[2], port, nextstatus)
                    stdscr.clear()
                    os.system('clear')
                    time.sleep(2)
                    nextstatus = ''
                    videolanguage = language()
                    sendtoserver(server[0], port, videolanguage)
                    sendtoserver(server[1], port, videolanguage)
                    selected = 0
                    countdownstarted = False
                    videoplaying = False
                    timeleft = 1
                    stdscr.refresh()
                    break
                if event == ord('n'):
                    break
    else:
        #stdscr.addstr(10,2, "Event: " + str(event))
        if countdownstarted == True:
            if timeleft > float(0):
                passedtime = time.time() - timestart
                timeleft = (3600 - passedtime) / 60
            else:
                stdscr.addstr(12,2, "Times up, everyone drowned :(     ", curses.color_pair(3))

        time.sleep(0.1)
        stdscr.refresh()
