#!/usr/bin/env/ python
# -*- coding: utf-8 -*-

import socket
import config
import os

server = '0.0.0.0' 
port = config.port

##---------------Send to Clients--------------------------

def sendtoserver(host, port, data):
    s = socket.socket()
    while True:
        try:
            s.connect((host, port))
            s.send(data)
            break
        except:
            continue            
    s.close()

##--------------Listen for Clients-----------------------

def listenforclients(host, port):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind((host,port))
    s.settimeout(0.01)
    try:
        s.listen(1)
        c, addr = s.accept()
        while True:
                data = c.recv(1024)
                if not data:
                    break
                nextstatus = data
                c.close()
                return nextstatus
    except:
        return ''

