#! /usr/bin/env/ python
# -*- coding: utf-8 -*-

import time
import curses
import locale
import os
import random
import connect
import socket
import config
nextstatus = 'play'

server = '0.0.0.0' 
port = config.port
mooltiplayerserver = config.server

locale.setlocale(locale.LC_ALL, '')

timeleft = 1
xpos = 92
ypos = 15
x = 0
y = 0
i = 0
nextstatus = ""
password = ""
acceptedkeys = '1','2','3','4','5','6','7','8','9','0' 
rows, columns = os.popen('stty size', 'r').read().split()
screenwidth = int(columns) - 20
screenheight = int(rows) - 5
clues = 'spacetime', 'dimensions', 'calculating', 'black holes'
starttime = False

screen = curses.initscr()
curses.cbreak(1)
screen.nodelay(1)
screen.keypad(1)
curses.noecho()
curses.curs_set(0)
curses.start_color()
curses.init_pair(1, curses.COLOR_BLACK, curses.COLOR_MAGENTA)
curses.init_pair(2, curses.COLOR_GREEN, curses.COLOR_BLACK)
curses.init_pair(3, curses.COLOR_MAGENTA, curses.COLOR_BLACK)

screen.clear()
event = ''

timestart = time.time()

while True:
    screen.refresh()
    serverstatus = connect.listenforclients(server, port)
    if serverstatus == "restart":
        password = ''
        timestart = time.time()
        serverstatus = ''
        starttime = False
    if serverstatus == 'play':
        screen.clear()
        starttime = True
        timestart = time.time()
    if timeleft < float(0):
        screen.addstr(39,97, 'You drowned! R.I.P.                                          ', curses.color_pair(3))
        timeleft = 1
        starttime = False
    if starttime == True:
        event = screen.getch()
        if event == 27:
            curses.nocbreak()
            screen.keypad(0)
            curses.echo()
            curses.endwin()
            print timeleft
            quit()
        if password == '19':
            screen.addstr(39,97, 'Put on your reading glasses.                                     ', curses.color_pair(3))
        if password == '1880':
            screen.addstr(39,97, 'At the end you draw the sword.                                   ', curses.color_pair(3))
        if password == '2418':
            screen.addstr(39,97, '18th of May.. keep forgetting the combination.                   ', curses.color_pair(3))
        if password == '2379':
            screen.addstr(39,97, 'Explosion of Vaskiluoto IV or was it in 3379.                    ', curses.color_pair(3))
        if password == '6666':
            screen.addstr(39,97, 'The devil is coming. Have you killed the mustache pirate?        ', curses.color_pair(3))
        if password == '1843':
            screen.addstr(39,97, 'The vase needs a flower.                                         ', curses.color_pair(3))
        if password == '4':
            screen.addstr(39,97, 'Flags in the middle. Do you get the riddle?                      ', curses.color_pair(3))
        if password == '3379':
            screen.addstr(39,97, 'Explosion of Vaskiluoto IV, if I remember correctly.             ', curses.color_pair(3))
        if password == '232741880':
            while True:
                nextstatus = 'timetravel'
                connect.sendtoserver(mooltiplayerserver[0], port, nextstatus)
                connect.sendtoserver(mooltiplayerserver[1], port, nextstatus)
                screen.addstr(39,97, 'Password correct.', curses.color_pair(3))
                screen.refresh()
                time.sleep(2)
                screen.addstr(40,97, 'Booting up Time Travel Pro...', curses.color_pair(3))
                screen.refresh()
                time.sleep(3)
                screen.addstr(41,97, 'Kernel Loaded.', curses.color_pair(3))
                screen.refresh()
                time.sleep(3)
                screen.addstr(42,97, 'Sending you back to the future...', curses.color_pair(3))
                screen.refresh()
                time.sleep(3)
                while True:
                    x = random.randint(10, screenwidth)
                    y = random.randint(4, screenheight)
                    i = random.randint(0, 3)
                    time.sleep(0.0001)
                    timetravel = str(time.time())
                    screen.addstr(y,x, ' 976738A ', curses.color_pair(3))
                    screen.refresh()
                    serverstatus = connect.listenforclients(server, port)
                    if serverstatus == 'restart':
                        break
                break

        #---Listen for keyboard numbers---
        for keys in acceptedkeys:
            if event == ord(keys):
                if len(password) < 11:
                    password = password + chr(event)
        if event == curses.KEY_BACKSPACE:
            if len(password) > 0:
                password = password[:-1] 

        #---Draw the countdown clock---
        else:
            screen.addstr(38,97, 'Please enter the password:            ', curses.color_pair(1))
            screen.addstr(38,124, password, curses.color_pair(1))
            passedtime = time.time() - timestart
            timeleft = (3600 - passedtime) / 60
            time.sleep(0.01)
            timecharlen = len(str(timeleft))
            char = 0
            nextchar = 0
            strtime = str(timeleft)
            if timeleft < float(0):
                strtime = '0.0000'
            M = u'\u2588'.encode('utf-8')
            while char < 6:
                if strtime[char] == '.':
                    screen.addstr(15 + ypos,10 + nextchar + xpos, '    ', curses.color_pair(3))
                    screen.addstr(16 + ypos,10 + nextchar + xpos, ' ' +M+M+ ' ', curses.color_pair(3))
                    screen.addstr(17 + ypos,10 + nextchar + xpos, '    ', curses.color_pair(3))
                    screen.addstr(18 + ypos,10 + nextchar + xpos, ' ' +M+M+ ' ', curses.color_pair(3))
                    screen.addstr(19 + ypos,10 + nextchar + xpos, '    ', curses.color_pair(3))
                if strtime[char] == '0':
                    screen.addstr(15 + ypos,10 + nextchar + xpos, M+M+M+M, curses.color_pair(3))
                    screen.addstr(16 + ypos,10 + nextchar + xpos, M+'  '+M, curses.color_pair(3))
                    screen.addstr(17 + ypos,10 + nextchar + xpos, M+'  '+M, curses.color_pair(3))
                    screen.addstr(18 + ypos,10 + nextchar + xpos, M+'  '+M, curses.color_pair(3))
                    screen.addstr(19 + ypos,10 + nextchar + xpos, M+M+M+M, curses.color_pair(3))
                if strtime[char] == '1':
                    screen.addstr(15 + ypos,10 + nextchar + xpos, '   '+M, curses.color_pair(3))
                    screen.addstr(16 + ypos,10 + nextchar + xpos, '   '+M, curses.color_pair(3))
                    screen.addstr(17 + ypos,10 + nextchar + xpos, '   '+M, curses.color_pair(3))
                    screen.addstr(18 + ypos,10 + nextchar + xpos, '   '+M, curses.color_pair(3))
                    screen.addstr(19 + ypos,10 + nextchar + xpos, '   '+M, curses.color_pair(3))
                if strtime[char] == '2':
                    screen.addstr(15 + ypos,10 + nextchar + xpos, M+M+M+M, curses.color_pair(3))
                    screen.addstr(16 + ypos,10 + nextchar + xpos, '   '+M, curses.color_pair(3))
                    screen.addstr(17 + ypos,10 + nextchar + xpos, M+M+M+M, curses.color_pair(3))
                    screen.addstr(18 + ypos,10 + nextchar + xpos, M+'   ', curses.color_pair(3))
                    screen.addstr(19 + ypos,10 + nextchar + xpos, M+M+M+M, curses.color_pair(3))
                if strtime[char] == '3':
                    screen.addstr(15 + ypos,10 + nextchar + xpos, M+M+M+M, curses.color_pair(3))
                    screen.addstr(16 + ypos,10 + nextchar + xpos, '   '+M, curses.color_pair(3))
                    screen.addstr(17 + ypos,10 + nextchar + xpos, M+M+M+M, curses.color_pair(3))
                    screen.addstr(18 + ypos,10 + nextchar + xpos, '   '+M, curses.color_pair(3))
                    screen.addstr(19 + ypos,10 + nextchar + xpos, M+M+M+M, curses.color_pair(3))
                if strtime[char] == '4':
                    screen.addstr(15 + ypos,10 + nextchar + xpos, M+'  '+M, curses.color_pair(3))
                    screen.addstr(16 + ypos,10 + nextchar + xpos, M+'  '+M, curses.color_pair(3))
                    screen.addstr(17 + ypos,10 + nextchar + xpos, M+M+M+M, curses.color_pair(3))
                    screen.addstr(18 + ypos,10 + nextchar + xpos, '   '+M, curses.color_pair(3))
                    screen.addstr(19 + ypos,10 + nextchar + xpos, '   '+M, curses.color_pair(3))
                if strtime[char] == '5':
                    screen.addstr(15 + ypos,10 + nextchar + xpos, M+M+M+M, curses.color_pair(3))
                    screen.addstr(16 + ypos,10 + nextchar + xpos, M+'   ', curses.color_pair(3))
                    screen.addstr(17 + ypos,10 + nextchar + xpos, M+M+M+M, curses.color_pair(3))
                    screen.addstr(18 + ypos,10 + nextchar + xpos, '   '+M, curses.color_pair(3))
                    screen.addstr(19 + ypos,10 + nextchar + xpos, M+M+M+M, curses.color_pair(3))
                if strtime[char] == '6':
                    screen.addstr(15 + ypos,10 + nextchar + xpos, M+M+M+M, curses.color_pair(3))
                    screen.addstr(16 + ypos,10 + nextchar + xpos, M+'   ', curses.color_pair(3))
                    screen.addstr(17 + ypos,10 + nextchar + xpos, M+M+M+M, curses.color_pair(3))
                    screen.addstr(18 + ypos,10 + nextchar + xpos, M+'  '+M, curses.color_pair(3))
                    screen.addstr(19 + ypos,10 + nextchar + xpos, M+M+M+M, curses.color_pair(3))
                if strtime[char] == '7':
                    screen.addstr(15 + ypos,10 + nextchar + xpos, M+M+M+M, curses.color_pair(3))
                    screen.addstr(16 + ypos,10 + nextchar + xpos, '   '+M, curses.color_pair(3))
                    screen.addstr(17 + ypos,10 + nextchar + xpos, '   '+M, curses.color_pair(3))
                    screen.addstr(18 + ypos,10 + nextchar + xpos, '   '+M, curses.color_pair(3))
                    screen.addstr(19 + ypos,10 + nextchar + xpos, '   '+M, curses.color_pair(3))
                if strtime[char] == '8':
                    screen.addstr(15 + ypos,10 + nextchar + xpos, M+M+M+M, curses.color_pair(3))
                    screen.addstr(16 + ypos,10 + nextchar + xpos, M+'  '+M, curses.color_pair(3))
                    screen.addstr(17 + ypos,10 + nextchar + xpos, M+M+M+M, curses.color_pair(3))
                    screen.addstr(18 + ypos,10 + nextchar + xpos, M+'  '+M, curses.color_pair(3))
                    screen.addstr(19 + ypos,10 + nextchar + xpos, M+M+M+M, curses.color_pair(3))
                if strtime[char] == '9':
                    screen.addstr(15 + ypos,10 + nextchar + xpos, M+M+M+M, curses.color_pair(3))
                    screen.addstr(16 + ypos,10 + nextchar + xpos, M+'  '+M, curses.color_pair(3))
                    screen.addstr(17 + ypos,10 + nextchar + xpos, M+M+M+M, curses.color_pair(3))
                    screen.addstr(18 + ypos,10 + nextchar + xpos, '   '+M, curses.color_pair(3))
                    screen.addstr(19 + ypos,10 + nextchar + xpos, '   '+M, curses.color_pair(3))
                char = char + 1
                nextchar = nextchar + 5
