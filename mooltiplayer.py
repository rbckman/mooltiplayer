#!/usr/bin/env/ python
# -*- coding: utf-8 -*-

import playlists
import subprocess
from subprocess import Popen
import os
import curses
import time
import pexpect
import pickle
import socket
import config

##---------free software------mooltiplayer--v05
##----------------------------------------------
## I'm a free software developer because I belive.
## One love ---------------------------------- 
##----  --- ----------------------server v0.1
##----   -  ---------       ----------------
##----      ---------      --------------
##---------------------   ----------
##---------------made-by-rbckman-----

global vlc, nextstatus, clientname, clientstatus

videofolder = playlists.videofolder
allplaylists = playlists.allplaylists
loopingvideos =  playlists.loopingvideos
controllable = playlists.controllable
server = '0.0.0.0' 
port = config.port

nextstatus = 'selectlang'
videolanguage = ''
videofiles = ''
videoplaying = False
gotonext = False
playvideos = ''
title = ''

os.system('clear')
#os.system('export DISPLAY=:0.0')


##--------------Listen for Clients-----------------------

def listenforclients(host, port):
    s = socket.socket()
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind((host,port))
    s.settimeout(2)
    try:
        s.listen(1)
        c, addr = s.accept()
        print "Connection from: " + str(addr)
        while True:
                data = c.recv(1024)
                if not data:
                    break
                nextstatus = data
                print nextstatus
                c.close()
                return nextstatus
    except:
        return ''

##--------------Main loop-------------------------

while True:

    print "------------Makeitout video server------------"
    print "------------waiting for controller------------"
    print ""
    print str(len(allplaylists) - 1) + ' playlists found'
    #print nextstatus
    #print videolanguage

    if nextstatus == 'selectlang':
        print "Waiting for language..."
        while videolanguage == '':
            videolanguage = listenforclients(server, port)
        nextstatus = ''

    nextstatus = listenforclients(server, port)

    #---Start vlc---
    if nextstatus == "play":
        os.system('python countdown.py >> /dev/tty1 &')
        print nextstatus
        print "ai, ai, capitain!"
        print "video language is " + videolanguage
        playvideos = '' #reset the playlist
        if videolanguage == 'english':
            playlistselected = allplaylists[1]
        elif videolanguage == 'svenska':
            playlistselected = allplaylists[2]
        elif videolanguage == 'suomi':
            playlistselected = allplaylists[3]
        else:
            print "strange, no videolanguage selected"
        for vid in playlistselected:
            playvideos = playvideos + videofolder + vid + ' '
        print 'videos found in selected playlist: ' + playvideos

        #---Using python pexpect to open up vlc and load the playlist, u can also use an api
        vlc = pexpect.spawn("/home/pi/vlc/vlc --control rc --vout omxil_vout --height=1080 --width=1920 --no-video-title-show " + playvideos + " --sout-keep --no-video-title-show")
        vlc.expect('>')
        vlc.sendline('volume 250') #---This is how u send a command with pexpect. respect!
        vlc.expect('>')
        vlc.sendline('loop off')
        vlc.expect('>')
        time.sleep(4)
        vlc.expect('>')
        vlc.sendline('get_title')
        time.sleep(1)
        vlc.expect('>')
        title = vlc.before
        print title
        nextstatus = ''
        videoplaying = True

    #---Crazyshit---
    if nextstatus == 'showtime':
        os.system('pkill -9 vlc')
        os.system('clear')
        videoplaying = False
        while True:
            nextstatus = listenforclients(server, port)
            if nextstatus == 'timetravel':
                break
            time.sleep(1)
        os.system('pkill -9 -f countdown.py')
        #time left here 

    #---Final scene---
    if nextstatus == 'timetravel':
        vlc = pexpect.spawn("/home/pi/vlc/vlc --control rc --vout omxil_vout --height=1080 --width=1920 --no-video-title-show part5.mp4 --sout-keep --no-video-title-show")
        vlc.expect('>')
        vlc.sendline('volume 250') #---This is how u send a command with pexpect. respect!
        vlc.expect('>')
        vlc.sendline('loop off')
        vlc.expect('>')
        time.sleep(4)
        videoplaying = True

    #---Send next to vlc---
    if videoplaying == True:
        if nextstatus == "next":
            if gotonext == True:
                print "ai, ai, capitain. playing the next video.."
                vlc.sendline('next')
                time.sleep(1)
                vlc.expect('>')
                print vlc.before
            nextstatus = ''

        #---Clues---
        if nextstatus == 'clue':
            print "got a clue.."
            #p1 = Popen(['aplay ', nextstatus, '.wav'])
            nextstatus = 'got a clue'

        if nextstatus == 'restart':
            print "restarting"
            os.system('pkill -9 -f countdown.py')
            os.system('pkill -9 vlc')
            videoplaying = False
            videolanguage = ''
            nextstatus = 'selectlang'

    #---Check if any videos are looping or controllable---config them in playlist.py
    if videoplaying == True:
        vlc.sendline('get_title')
        time.sleep(1)
        vlc.expect('>')
        title = vlc.before
        print title
        if any(s in title for s in loopingvideos): #check if a string from a list is in a string
            vlc.sendline('repeat on')
            vlc.expect('>')
            print 'looping video'
        else:
            vlc.sendline('repeat off')
            vlc.expect('>')
            print 'looping is off'
        if any(s in title for s in controllable): #same here!
            print 'video is controllable'
            gotonext = True
        else:
            print 'video is not controllabe'
            gotonext = False

    time.sleep(1)
    os.system('clear') ##---this is how i learnt to program---rbckman 
