#!/usr/bin/env python
# -*- coding: utf-8 -*-

##<<<<mooltiplayer playlists>>>>

##------Setup your videofolder, playlists and looping videos. simple-----
videofolder = '/home/pi/mooltiplayer/'
playlist1 = ['english-part1', 'part2.mp4', 'english-part3.mp4', 'part4.mp4', 'part5.mp4']
playlist2 = ['swedish-part1.mp4', 'part2.mp4', 'swedish-part3.mp4', 'part4.mp4', 'part5.mp4']
playlist3 = ['finnish-part1.mp4', 'part2.mp4', 'finnish-part3.mp4', 'part4.mp4', 'part5.mp4']
loopingvideos = 'part2.mp4', 'part4.mp4'
controllable = 'part2.mp4', 'part4.mp4'

empty = []
##------Add your playlists here, leave empty, simple------
allplaylists = empty, playlist1, playlist2, playlist3
